package ru.t1.aksenova.tm.command.system;

import ru.t1.aksenova.tm.api.service.ICommandService;
import ru.t1.aksenova.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
